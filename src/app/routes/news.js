const dbConnection = require('../../config/dbConnection');
module.exports = app =>{
    const connection = dbConnection();
    app.get('/news', (req, res) => {
        connection.query('SELECT * FROM reviews_generals_ekomi', (err, result) => {
          res.render('view', {
            news: result
          });
        });
      });

      app.post('/news', (req, res) => {
        const { title, news } = req.body;
        connection.query('INSERT INTO news SET ? ',
          {
            title,
            news
          }
        , (err, result) => {
          res.redirect('/news');
        });
      });
      app.get('/edit/:id', (req, res) => {
        connection.query('SELECT comment, id, raiting FROM reviews_generals_ekomi WHERE id = ?', [Number(req.params.id)], (err, result) => {
          //console.log(result);
          res.render('edit', {
            news: result,
            title: "Editar registro"
          });
        });
      });
      app.get('/bid_delete/:id', async (req, res) => {
        let bid_no = req.params.id;
        console.log("Delete registro con id",bid_no);
        let qrBids= "DELETE FROM reviews_generals_ekomi WHERE id = ?"  
        await connection.query( "SET SQL_SAFE_UPDATES = 0");
        await connection.query( qrBids, [bid_no], (err, result) => {
         if (err) {
           res.send(err)
           res.redirect('/news');
         } else { 
           console.log("con éxito");
           res.redirect('/news');
           //res.json({success : true})
         }
        });
       })

       app.post('/edit_registro/:id', async (req, res) => {
        console.log("LLegando para update",req.params.id);
        const {comment, raiting} = req.body;
        const dat = {
          comment,
          raiting
        }
        //console.log(dat);
        //var param = [req.body, Number(req.params.id)];
        await connection.query('UPDATE reviews_generals_ekomi SET ? WHERE id = ? ', [dat, Number(req.params.id)], (err, result) => {
          if (err) {
            res.send(err)
            res.redirect('/news');
          } else {
            console.log("con éxito");
            res.redirect('/news');
            //res.json({success : true})
          }
          });
          //console.log(rs);
      });
};
